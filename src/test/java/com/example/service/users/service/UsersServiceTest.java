package com.example.service.users.service;

import com.example.service.users.dao.UsersDAO;
import com.example.service.users.models.User;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UsersServiceTest {


    @Mock
    private UsersDAO usersDAO;

    @InjectMocks
    private UsersService usersService;

    @Test
    void handleSaveUser() {
        //Given
        User user = new User();
        user.setEmail("angel.ram@email.com");
        user.setDateOfBirth("28-01-1991");
        user.setStatus(false);

        when(usersDAO.getUserByEmail("angel.ram@email.com"))
                .thenReturn(User.builder()
                        .id("1")
                        .email("angel.ram@email.com")
                        .build());

        //When
        User saveUser = usersService.handleSaveUser(user);

        verify(usersDAO).saveUser(user);


        //Then
        assertNotNull(saveUser);
        assertEquals("1", saveUser.getId());
        assertEquals("angel.ram@email.com", saveUser.getEmail());

    }

    @Test
    void handleSaveUserNoEmail() {
        //Given
        User user = new User();
        user.setEmail("");
        user.setDateOfBirth("28-01-1991");
        user.setStatus(false);


        //When
        User saveUser = usersService.handleSaveUser(user);
        verifyNoInteractions(usersDAO);


        //Then
        assertNull(saveUser);

    }
}