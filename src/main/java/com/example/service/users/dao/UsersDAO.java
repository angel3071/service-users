package com.example.service.users.dao;

import com.example.service.users.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.List;

@Repository
public class UsersDAO {


    @Autowired
    JdbcTemplate jdbcTemplate;


    public List<User> getAllUsers() {

        return Collections.emptyList();
    }

    public User getUser(String id) {

        return null;
    }

    public User getUserByEmail(String email) {

        return (User) jdbcTemplate.queryForObject(
                "SELECT id, email, date_of_birth FROM users WHERE email = ?",
                new Object[]{email},
                new BeanPropertyRowMapper(User.class));
    }

    public void saveUser(User user) {

        jdbcTemplate.update(
                "INSERT INTO users " +
                        "(email, date_of_birth, status) " +
                        "VALUES " +
                        "(?, STR_TO_DATE(?, '%d-%m-%Y'), ?)",
                user.getEmail(),
                user.getDateOfBirth(),
                user.isStatus()
                );

    }
}
