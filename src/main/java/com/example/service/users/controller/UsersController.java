package com.example.service.users.controller;


import com.example.service.users.models.User;
import com.example.service.users.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UsersController {

    @Autowired
    private UsersService usersService;


    @GetMapping("/users")
    public List<User> getUsers() {
        return List.of(
                new User("1", "some@email.com", "1991-01-01", true),
                new User("2", "some2@email.com", "1991-01-02", false)
        );
    }

    @GetMapping("/users/{id}")
    public User getUser(String id) {
        return new User();
    }

    @PostMapping("/users")
    public User saveUser(@RequestBody User user) {

        System.out.println(user);
        return usersService.handleSaveUser(user);
    }
}
