package com.example.service.users.service;

import com.example.service.users.dao.UsersDAO;
import com.example.service.users.models.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Slf4j
@Service
public class UsersService {


    @Autowired
    private UsersDAO usersDAO;

    public User handleSaveUser(User user) {

        log.debug("Attempting to save user for: {}", user);

        if (StringUtils.isEmpty(user.getEmail()))
            return null;

        usersDAO.saveUser(user);
        log.debug("User successfully saved for: {}", user);

        return usersDAO.getUserByEmail(user.getEmail());
    }

    private void doNothing() {

    }

}
