USE demo;
INSERT INTO users (email, date_of_birth, status) VALUES ('some@email.com', STR_TO_DATE('1-01-2012', '%d-%m-%Y'), 1);
INSERT INTO users (email, date_of_birth, status) VALUES ('another@email.com', STR_TO_DATE('1-01-1990', '%d-%m-%Y'), 0);
INSERT INTO users (email, date_of_birth, status) VALUES ('something@email.com', STR_TO_DATE('1-01-2005', '%d-%m-%Y'), 0);
INSERT INTO users (email, date_of_birth, status) VALUES ('other@email.com', STR_TO_DATE('1-02-1985', '%d-%m-%Y'), 1);